;;Assignment 1
;;Author: Tyler Arseneault
;;Due: January 23, 2018

;;---Question 1---;;

;;Poly-add
;;Adds two polynomials
;;Param: list a, list b
(defun poly-add (a b) 
    (let ((l '()))
        (cond
            ((null a) b)
            ((null b) a)
            (t (append (cons (cons (caar a) (cons (+ (cadar a) (cadar b)) '())) (poly-add (cdr a)(cdr b))) l))
        )
    )     
)

;;Poly-sub
;;Subtracts two polynomials
;;Param: list a, list b
(defun poly-sub (a b)
    (let ((l '()))
        (cond
            ((null a) b)
            ((null b) a)
            (t (append (cons (cons (caar a) (cons (- (cadar a) (cadar b)) '())) (poly-sub (cdr a)(cdr b))) l))
        )
    )
)

;;Poly-mul
;;Multiplies two polynomials
;;Param: list a, list b
;;NOT DONE
(defun poly-mul (a b)
    (cond
        ((null b) a)
        (t (poly-add a b))
    )
)

;;Poly-der
;;Finds the deriviative of a polynomial
;;Param: list a
(defun poly-der (a)
    (let ((l '()))
        (cond
            ((= (caar a) 0) nil)
            (t (append (cons (cons (- (caar a) 1) (cons (* (cadar a) (caar a)) '())) (poly-der (cdr a))) l))
        )
    )
)

;;Poly-prim
;;Finds the anti-deriviative of a polynomial
;;Param: list a
(defun poly-prim (a)
    (let ((l '()))
        (cond
            ((null a) nil)
            (t (append (cons (cons (+ (caar a) 1) (cons (/ (cadar a) (+ (caar a) 1)) '())) (poly-prim (cdr a))) l))
        )
    )
)

;;Poly-val
;;Finds the value of a polynomial
;;Param: list a, value x
(defun poly-val (a x)
    (cond 
        ((null a) 0)
        (t (+ (* (cadar a)(exponent (caar a) x)) (poly-val(cdr a) x)))
    )
)

;;Exponent
;;Helper function for Poly-val
;;Calculates the exponential value of x
;;Param: Exponent number e, number x
(defun exponent (e x)
    (cond
        ((= e 0) 1)
        (t (* x (exponent (- e 1) x)))
    )
)

;;---Question 2 ---;;

;;Deep-subst
;;Replaces an instance of x in a list l with y
;;Param: atom x, atom y, list l
(defun deep-subst (x y l)
    (cond
        ((null (car l)) nil)
        ((and (eq (car l) x)(atom (car l))) (cons y (deep-subst x y (cdr l))))
        ((and (listp (car l))(eq (caar l) x)) (cons (cons y (deep-subst x y (cadr l))) '()))
        (t (cons (car l)(deep-subst x y (cdr l))))
    )
)

;;---Question 3---;;

;;count-sub-list-occ
;;Counts the number of times a sublist occurs in a list
;;Param: List l1, list l2
(defun count-sub-list-occ (l1 l2)   
    (cond   
        ((null l2) 0)
        ((equal l1 (cons (car l2) (cons (cadr l2) '()))) (+ 1 (count-sub-list-occ l1 (cdr l2))))
        (t (+ 0 (count-sub-list-occ l1 (cdr l2))))
    )
)

;;---Question 4---;;

;;gen-pass
;;Generates a password using a sentence, server name, and replaces atoms occurring with another atom
;;Param: List server, list sen, list sub
(defun gen-pass (server sen sub)
    (subst-list (sn-list server (password sen)) sub)
)

;;password
;;Genrates the initial password using a sentence
;;Param: list sen
(defun password (sen) 
    (cond
        ((null sen) sen)
        (t (cons (caar sen)(password(cdr sen))))
    )
)

;;sn-list
;;Adds the first atom of the server name to the start of the generated password, and appends the last atom of the server name to the password
;;Param: list server, list pass
(defun sn-list (server pass) 
    (append (cons (caar server) pass) (cons (last-atom (last-atom server)) '()) )
)

;;last-atom
;;Finds the last atom of a list of lists
;;Param: list l
(defun last-atom (l) 
    (cond
        ((null (cdr l)) (car l))
        (t (last-atom (cdr l)))
    )
)

;;subst-list
;;Substitutes an atom in a list with another atom
;;Param: list pass, list of lists sub
(defun subst-list (pass sub)
    (cond 
        ((null sub) pass)
        (t (subst-list (deep-subst (caar sub)(cadar sub) pass)(cdr sub)))
    )
)
